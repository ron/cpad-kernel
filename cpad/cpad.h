/* ioctl external interface */
// these numbers will change when the driver becomes official ?
#define CPAD_VERSION		_IOR('U', 0x71, int)			/* get driver version */
#define CPAD_CGID		_IOR('U', 0x72, short[4])		/* get device ID */
#define CPAD_RESET		_IO('U', 0x73)  			/* reset usb device cpad */
#define CPAD_WLIGHT		_IOW('U', 0x74, char)			/* set backlight state */
#define CPAD_WIMAGEL		_IOW('U', 0x75, char[30])		/* write an image line */
#define CPAD_WLCD		_IOW('U', 0x76, char)			/* set LCD state */
#define CPAD_RLIGHT		_IO('U', 0x77)  			/* read backlight state */
#define CPAD_RLCD		_IO('U', 0x78)  			/* read LCD state */
#define CPAD_FLASH		_IOW('U', 0x79, int)			/* flash backlight */
#define CPAD_SET_STROKE		_IOW('U', 0x7a, int)			/* change mouse motion sensitivity */
#define CPAD_SET_SENS		_IOW('U', 0x80, int)			/* change tap sensitivity */
#define CPAD_SET_ABS		_IOW('U', 0x81, char)			/* change abs/rel output mode */

/* 
 *  non-lcd controller cPad commands apear to be of the format 
 *   
 *  <select> <function> <state>
 *
 */
/* cPad not-lcd-controller select */
#define SEL_CPAD      0x01

/* observed functions */
#define CPAD_W_ROM    0x01
#define CPAD_R_ROM    0x02
#define CPAD_W_LIGHT  0x03
#define CPAD_R_LIGHT  0x04
#define CPAD_W_LCD    0x05
#define CPAD_R_LCD    0x06
#define CPAD_RSRVD    0x07


/* 
 * the lcd controller is a Seiko/Epson 1335
 *
 * commands appear to be of the format
 *
 *  <select> <1335 command> [<data> ...]
 *
 *   where <data> are the 1335 command parameters, reversed.
 *
 */


/* lcd controller select */
#define SEL_1335     0x02
 
/* 1335 commands */
#define SYSSET_1335  0x40

#define MWRITE_1335  0x42
#define MREAD_1335   0x43
#define SCROLL_1335  0x44
#define CSRW_1335    0x46
#define CSRR_1335    0x47

#define DISPOFF_1335 0x58
#define DISP_1335    0x59
#define HDOTSCR_1335 0x5a
#define OVLAY_1335   0x5b

#define CSRF_1335    0x5d

