/*
 * USB Syn_cPad driver - 0.7
 *
 * [USB Skeleton driver code] Copyright (c) 2001 Greg Kroah-Hartman (greg@kroah.com)
 * Synaptics cPad code copyright (c) 2002 Rob Miller (rob@inpharmatica . co . uk)
 *                               (c) 2003 Ron Lee (ron@debian.org)
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *
 * synaptics cPad :
 *
 * This is a driver for the Synaptics cPad, based on the usb-skeleton.c 
 * code from kernel 2.4.18, plus usbmouse.c and wacom.c.  All 3 mouse 
 * buttons on the Toshiba 5100-501 work, as does tap-to-click.  You can
 * use the file ops to write to the LCD controller 
 * (mknod /dev/usb/cpad0 c 180 66 or use devfs), see a 1335 datasheet 
 * for info and usr_cpad.c for some examples.
 *
 * see the defines MIN_PRESS, SCALE, TAP_TIME, TND_TIME and below
 * to vary the mouse behaviour, sensitivity, etc.
 *
 * 0.3: ioctl to flash backlight, adjust sensitivity (David Banas),
 *      comments, other fixes
 * 0.2: add ioctls; dev file io goes to 1335
 *
 * TODO:
 *
 *   from usb-skeleton:
 *	- fix urb->status race condition in write sequence
 *	- move minor_table to a dynamic list.
 *
 * History:
 *   07 Sept 2002 - 0.1a (alpha) release
 *   08 Sept 2002 - 0.1  build separate from kernel source, minor cleaning.
 *   13 Nov  2002 - 0.2  1335 LCD controller commands via file i/o, ioctls for remainder
 *      Dec  2002 - 0.3  flash, sensitivity (dbanas) ioctls, fixes
 *   19 Dec  2002 - 0.4  Added tap-n-drag, adjust motion sensitivity (Michael Lloyd-Hart)
 *   26 Jan  2003 - 0.5  Fixed motion sensitivity bug (Michael Lloyd-Hart)
 *   08 Mar  2003 - 0.6  Blame Ron for new bugs in this one...
 *		  -	 Added Abs/Rel mode ioctl.  Note that this can be dangerous
 *			 since it changes guarantees made to running processes, but
 *			 it seems ok with current X and gpm and is invaluable when in
 *			 maintainence mode.
 *		  -	 Replaced deprecated macros with their function equivalents.
 *		  -	 Removed unused eu_count from cpad_eat_urb.
 *		  -	 Fixed gcc3.2 warnings about sdifx = sdifx operation and
 *			 changed (c99) __FUNCTION__ semantics.
 *		  -	 Linus friendly indenting :-)
 *   24 Mar	  -	 Removed unused rel_delay and added init and del for queue.delay
 *		  -	 Tweaked x,y ABSMIN/MAX to something closer to reality, this
 *			 may need more fine tuning, or something adaptive.
 *		  -	 Enabled absfuzz for x,y,pressure.  It appears to work very
 *		  	 nicely in 2.4 and saves a lot of userspace noise filtering.
 *		  -	 Send finger up event in abs mode (pressure == 0).
 *		  -	 Enable sensitivity setting for abs mode.
 *		  -	 Fix check for 'W' in lsnybble of byte 0 of the hardware packet.
 *		         Stated values are:
 *		         	0 = two finger touch
 *		         	1 = three+ finger touch
 *		         	2 = pen touch (not supported in 5205 intalled cpad)
 *		         	3 = Reserved.
 *		           4 -  7 = Normal finger touch.
 *		           8 - 14 = Very wide finger or palm.
 *		               15 = The cat is asleep on it.
 *		         In theory you can use these to discriminate palm touches..
 *		         in practice, you're still going to experience some fratricide,
 *		         or pay some other filtering cost.
 *   13 Apr	  - 0.7	 Applied slightly modified patch from Dean Pentcheff to smooth
 *   			 relative motion.
 *   		  -	 Bumped the version again since Rob released 0.6 with Dean's patch.
 *   23 Apr	  -	 Fixed flash timer bug.
 */

#include <linux/config.h>

#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif
#ifndef CONFIG_MODVERSIONS
#  define CONFIG_MODVERSIONS
#endif

/* Standard headers for LKMs */
#include <linux/modversions.h> 
#include <linux/module.h>  

#include <linux/kernel.h> /* printk() */

#include <linux/slab.h> /* kmalloc() */
#include <linux/signal.h>
#include <linux/poll.h>
#include <linux/timer.h>  /* timer callback for tap to click */

#include <linux/fcntl.h>
#include <linux/devfs_fs_kernel.h>

#include <linux/module.h>

/* usb stuff */
#include <linux/input.h>
#include <linux/usb.h>

#include "cpad.h"


//#define CONFIG_USB_DEBUG 1

#ifdef CONFIG_USB_DEBUG
	static int debug = 1;
#else
	static int debug;
#endif


/* Use our own dbg macro */
#undef dbg
#define dbg(format, arg...)		\
	do { if (debug) 		\
	     printk(KERN_DEBUG __FILE__ ": %s - " format "\n" , __FUNCTION__ , ## arg); } \
	while (0)


/* Version Information */
#define DRIVER_VERSION "v0.7"
#define DRIVER_AUTHOR "Rob Miller rob .at. janerob.com"
#define DRIVER_DESC "USB Synaptics cPad Driver"
#define DRIVER_NUM 7

/* Module parameters */
MODULE_PARM(debug, "i");
MODULE_PARM_DESC(debug, "Debug enabled or not");

static int absmouse=0;
MODULE_PARM(absmouse, "i");
MODULE_PARM_DESC(absmouse, "abs mouse or not");

/* Synaptics cPad */
#define USB_CPAD_VENDOR_ID	0x06cb
#define USB_CPAD_PRODUCT_ID	0x0003

/* table of devices that work with this driver */
static struct usb_device_id cpad_table [] = {
	{ USB_DEVICE(USB_CPAD_VENDOR_ID, USB_CPAD_PRODUCT_ID) },
	{ }					/* Terminating entry */
};

MODULE_DEVICE_TABLE (usb, cpad_table);



/* minor device 66 assigned by LANANA Nov 2002 */
#define USB_CPAD_MINOR_BASE	66	

/* we can have up to this number of device plugged in at once */
#define MAX_DEVICES		16

// minimum pressure reading for mouse response
#define MIN_PRESS		0x30

// default scaling for finger motion to pointer motion
#define SCALE			3

// max time finger down to count as a tap in microseconds
#define TAP_TIME		200000

// after a finger-down event, ignore motions for this long - minimises
// annoying jumpy behaviour
#define JITTER_TIME		(TAP_TIME/4)

// max time between tap and drag to count as a tap-n-drag in $\mu$s
// Increase only with caution: you may come to grief with left button autorepeating
#define TND_TIME		250000
#define TND_JIFFIES		(TND_TIME / 10000)

// max no. of button events we can queue up (must be 2^n)
#define NEVENT			(0x01<<4)

// Button press events
#define DOWN			1
#define UP			0

/* Structure to hold all of our device specific stuff */
struct usb_cpad {
	struct usb_device *	udev;			/* save off the usb device pointer */
	struct usb_interface *	interface;		/* the interface for this device */
	devfs_handle_t		devfs;			/* devfs device node */
	unsigned char		minor;			/* the starting minor number for this device */
	unsigned char		num_ports;		/* the number of ports this device has */
	char			num_interrupt_in;	/* number of interrupt in endpoints we have */
	char			num_bulk_in;		/* number of bulk in endpoints we have */
	char			num_bulk_out;		/* number of bulk out endpoints we have */

	unsigned char *		bulk_in_buffer;		/* the buffer to receive data */
	int			bulk_in_size;		/* the size of the receive buffer */
	__u8			bulk_in_endpointAddr;	/* the address of the bulk in endpoint */
  
	unsigned char *		bulk_out_buffer;	/* the buffer to send data */
	int			bulk_out_size;		/* the size of the send buffer */
	struct urb *		write_urb;		/* the urb used to send data */
	__u8			bulk_out_endpointAddr;	/* the address of the bulk out endpoint */
  
	unsigned char *		mdata;			/* cpad mouse input buffer */
	char			mname[128];		/* cpad mouse name */
	int			mopen;			/* mouse input device open counter */
	struct urb *		irq_urb;		/* mouse movement urb */
	__u8			irq_in_endpointAddr;	/* the address of the bulk out endpoint */
	struct input_dev	idev;			/* input subsystem device */

	struct timer_list	flash_delay;		/* time to turn off backlight */

	struct tq_struct	tqueue;			/* task queue for line discipline waking up */
	int			open_count;		/* number of times this port has been opened */
	struct semaphore	sem;			/* locks this structure */

	/* Persistent data for rel mouse emulation */

	__u32			lastx,lasty;		/* keep track to emulate rel mouse */
	int			touch;			/* no relmouse jump on finger lift */
	struct timeval		tv;			/* length of tap to click */
	int			tap;			/* remember we've had a tap */
};


struct fifo{
	struct usb_cpad		*device[NEVENT];
	struct timer_list	delay[NEVENT];
	unsigned int		action[NEVENT];
	unsigned int		wpoint;
};


/* the global usb devfs handle */
extern devfs_handle_t usb_devfs_handle;


/* local function prototypes */
static ssize_t cpad_read	(struct file *file, char *buffer, size_t count, loff_t *ppos);
//static ssize_t cpad_write	(struct file *file, const char *buffer, size_t count, loff_t *ppos);
static ssize_t cpad_1335_write	(struct file *file, const char *buffer, size_t count, loff_t *ppos);
static int cpad_ioctl		(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);
static int cpad_open		(struct inode *inode, struct file *file);
static int cpad_release		(struct inode *inode, struct file *file);
	
static void * cpad_probe	(struct usb_device *dev, unsigned int ifnum, const struct usb_device_id *id);
static void cpad_disconnect	(struct usb_device *dev, void *ptr);

static void cpad_write_bulk_callback	(struct urb *urb);


/* array of pointers to our devices that are currently connected */
static struct usb_cpad		*minor_table[MAX_DEVICES];

/* lock to protect the minor_table structure */
static DECLARE_MUTEX (minor_table_mutex);

// Event queue
static struct fifo	queue;

// Delayed release event
static unsigned int	del_rel;

/*
 * File operations needed when we register this driver.
 * This assumes that this driver NEEDS file operations,
 * of course, which means that the driver is expected
 * to have a node in the /dev directory. If the USB
 * device were for a network interface then the driver
 * would use "struct net_driver" instead, and a serial
 * device would use "struct tty_driver". 
 */
static struct file_operations cpad_fops = {
	/*
	 * The owner field is part of the module-locking
	 * mechanism. The idea is that the kernel knows
	 * which module to increment the use-counter of
	 * BEFORE it calls the device's open() function.
	 * This also means that the kernel can decrement
	 * the use-counter again before calling release()
	 * or should the open() function fail.
	 *
	 * Not all device structures have an "owner" field
	 * yet. "struct file_operations" and "struct net_device"
	 * do, while "struct tty_driver" does not. If the struct
	 * has an "owner" field, then initialize it to the value
	 * THIS_MODULE and the kernel will handle all module
	 * locking for you automatically. Otherwise, you must
	 * increment the use-counter in the open() function
	 * and decrement it again in the release() function
	 * yourself.
	 */
	owner:		THIS_MODULE,

	read:		cpad_read,
	write:		cpad_1335_write,
	ioctl:		cpad_ioctl,
	open:		cpad_open,
	release:	cpad_release,
};      


/* usb specific object needed to register this driver with the usb subsystem */
static struct usb_driver cpad_driver = {
	name:		"syn_cpad",
	probe:		cpad_probe,
	disconnect:	cpad_disconnect,
	fops:		&cpad_fops,
	minor:		USB_CPAD_MINOR_BASE,
	id_table:	cpad_table,
};

/* Changing constant to variable, in order to allow dynamic changing of tap and 
 * motion sensitivities */
static int		min_press = MIN_PRESS;
static int		scale = SCALE;
static int 		addon = 0;




/**
 *	usb_cpad_debug_data
 */
static inline void usb_cpad_debug_data (const char *function, int size, const unsigned char *data)/*{{{*/
{
	int i;

	if (!debug) return;
	
	printk (KERN_DEBUG __FILE__": %s - length = %d, data = ", function, size);
	for (i = 0; i < size; ++i) {
		printk ("%.2x ", data[i]);
	}
	printk ("\n");
}
/*}}}*/

/**
 *	cpad_delete
 */
static inline void cpad_delete (struct usb_cpad *dev)/*{{{*/
{
	minor_table[dev->minor] = NULL;
	if (dev->mdata != NULL)
	        kfree (dev->mdata);
	if (dev->bulk_in_buffer != NULL)
		kfree (dev->bulk_in_buffer);
	if (dev->bulk_out_buffer != NULL)
		kfree (dev->bulk_out_buffer);
	if (dev->write_urb != NULL)
		usb_free_urb (dev->write_urb);
	if (dev->irq_urb != NULL)
		usb_unlink_urb (dev->write_urb);
	if (dev->idev.private != NULL) 
		input_unregister_device(&dev->idev);
	kfree (dev);
}
/*}}}*/

/**
 *	cpad_open
 */
static int cpad_open (struct inode *inode, struct file *file)/*{{{*/
{
	struct usb_cpad *dev = NULL;
	int subminor;
	int retval = 0;
	
	dbg("Opening device");

	subminor = MINOR (inode->i_rdev) - USB_CPAD_MINOR_BASE;
	if ((subminor < 0) ||
	    (subminor >= MAX_DEVICES)) {
		return -ENODEV;
	}

	/* Increment our usage count for the module.
	 * This is redundant here, because "struct file_operations"
	 * has an "owner" field. This line is included here soley as
	 * a reference for drivers using lesser structures... ;-)
	 */
	MOD_INC_USE_COUNT;

	/* lock our minor table and get our local data for this minor */
	down (&minor_table_mutex);
	dev = minor_table[subminor];
	if (dev == NULL) {
		up (&minor_table_mutex);
		MOD_DEC_USE_COUNT;
		return -ENODEV;
	}

	/* lock this device */
	down (&dev->sem);

	/* unlock the minor table */
	up (&minor_table_mutex);

	/* increment our usage count for the driver */
	++dev->open_count;

	/* save our object in the file's private structure */
	file->private_data = dev;

	/* unlock this device */
	up (&dev->sem);

	return retval;
}
/*}}}*/

/**
 *	cpad_release
 */
static int cpad_release (struct inode *inode, struct file *file)/*{{{*/
{
	struct usb_cpad *dev;
	int retval = 0;

	dev = (struct usb_cpad *)file->private_data;
	if (dev == NULL) {
		dbg ("object is NULL");
		return -ENODEV;
	}

	dbg("minor %d", dev->minor);

	/* lock our minor table */
	down (&minor_table_mutex);

	/* lock our device */
	down (&dev->sem);

	if (dev->open_count <= 0) {
		dbg ("device not opened");
		retval = -ENODEV;
		goto exit_not_opened;
	}

	if (dev->udev == NULL) {
		/* the device was unplugged before the file was released */
		up (&dev->sem);
		cpad_delete (dev);
		up (&minor_table_mutex);
		MOD_DEC_USE_COUNT;
		return 0;
	}

	/* cut per Oliver Neukum [linux-usb-devel] Bug in usb-skeleton

	"there's a bad one in usb-skeleton.c. It assumes that release is
	called on every close. That's not the case and decrementing a
	counter in release is a bad bug.
	No wonder we get buggy drivers.

	This is against 2.5BK. Greg, please apply."



	/ * decrement our usage count for the device * /
	--dev->open_count;
	if (dev->open_count <= 0) {
		/ * shutdown any bulk writes that might be going on * /
		usb_unlink_urb (dev->write_urb);
		dev->open_count = 0;
	}
	*/

	/* shutdown any bulk writes that might be going on */
	/* bulk reads cannot be active, because we have mutual
	   exclusion with read due to the device semaphore */
	usb_unlink_urb (dev->write_urb);
	dev->open_count = 0;


	/* end of Oliver Neukom patch */

	/* decrement our usage count for the module */
	MOD_DEC_USE_COUNT;

exit_not_opened:
	up (&dev->sem);
	up (&minor_table_mutex);

	return retval;
}
/*}}}*/

/**
 *	cpad_read
 */
static ssize_t cpad_read (struct file *file, char *buffer, size_t count, loff_t *ppos)/*{{{*/
{
	struct usb_cpad *dev;
	int retval = 0;

	dev = (struct usb_cpad *)file->private_data;
	
	dbg("minor %d, count = %d", dev->minor, count);

	/* lock this object */
	down (&dev->sem);

	/* verify that the device wasn't unplugged */
	if (dev->udev == NULL) {
		up (&dev->sem);
		return -ENODEV;
	}
	
	/* do an immediate bulk read to get data from the device */
	retval = usb_bulk_msg (dev->udev,
			       usb_rcvbulkpipe (dev->udev, 
						dev->bulk_in_endpointAddr),
			       dev->bulk_in_buffer, dev->bulk_in_size,
			       &count, HZ*10);

	/* if the read was successful, copy the data to userspace */
	if (!retval) {
		if (copy_to_user (buffer, dev->bulk_in_buffer, count))
			retval = -EFAULT;
		else
			retval = count;
	}
	
	/* unlock the device */
	up (&dev->sem);
	return retval;
}
/*}}}*/

/**
 *	cpad_1335_write
 *
 *      write a command to the 1335.  The first byte must be 0x02 to select the 1335,
 *      the second byte is the 1335 command, remaining parameter bytes are *reversed*
 *      to send to cPad.  to handle this we require/limit 
 *          dev->bulk_out_size = 32 (CPAD_BUFMAX)
 *      this routine is called for file write by user
 *      this is where we reverse the parameter list to the 1335
 */

#define CPAD_BUFMAX 32
static ssize_t cpad_1335_write (struct file *file, const char *buffer, size_t count, loff_t *ppos)/*{{{*/
{
	int retval = 0;
	int wcount = 0;
	struct usb_cpad *dev;
	ssize_t bytes_written = 0;
	unsigned char tmpbuf[CPAD_BUFMAX] = {0,0,0,0,0,0,0,0,
					     0,0,0,0,0,0,0,0,
					     0,0,0,0,0,0,0,0,
					     0,0,0,0,0,0,0,0} ; //v.ugly! 

	dev = (struct usb_cpad *)file->private_data;

	dbg("minor %d, count = %d", dev->minor, count);

	/* lock this object */
	down (&dev->sem);

	/* verify that the device wasn't unplugged */
	if (dev->udev == NULL) {
		retval = -ENODEV;
		goto exit;
	}

	/* verify that we actually have some data to write */
	if (count == 0) {
		dbg("1335 write request of 0 bytes");  // must have at least command
		goto exit;
	}
  
	/* see if we are already in the middle of a write */
	if (dev->write_urb->status == -EINPROGRESS) {
		dbg ("already writing");
		if (file->f_flags & O_NONBLOCK) 
			retval = -EAGAIN;
		else 
			retval = -EBUSY; // fixme: should wait for completion here

		goto exit;
	}
  
	/* we can only write up to *hardcoded limit*
	 * bytes_written = (count > (size_t) dev->bulk_out_size) ? dev->bulk_out_size : count;
	 */
	bytes_written = (count > (size_t) CPAD_BUFMAX-1) ? CPAD_BUFMAX-1 : count;

	/* copy the data from userspace into our local buffer */

	if (copy_from_user(tmpbuf, buffer, bytes_written)) {
		retval = -EFAULT;
		goto exit;
	} else {
		/* here we set the 1335 select, 1335 command, and reverse the params */
		int i;

		((char *) dev->write_urb->transfer_buffer)[0] = SEL_1335;
		((char *) dev->write_urb->transfer_buffer)[1] = tmpbuf[0];

		for (i=bytes_written,wcount=2;i>1 && wcount<CPAD_BUFMAX;) 
			((char *) dev->write_urb->transfer_buffer)[wcount++] = tmpbuf[--i];
	}

	usb_cpad_debug_data (__FUNCTION__, bytes_written, dev->write_urb->transfer_buffer);

	/* set up our urb */
	usb_fill_bulk_urb(dev->write_urb,
			  dev->udev, 
			  usb_sndbulkpipe(dev->udev, dev->bulk_out_endpointAddr),
			  dev->write_urb->transfer_buffer,
			  bytes_written + 1,
			  cpad_write_bulk_callback,
			  dev);

	/* send the data out the bulk port */
	retval = usb_submit_urb(dev->write_urb);
	if (retval) {
		err("%s - failed submitting write urb, error %d", __FUNCTION__, retval);
	}
	else {
		retval = bytes_written;
	}

exit:  /* unlock the device */
	up (&dev->sem);
  
	//printk(KERN_DEBUG "cpad 1335 bulk write %d bytes\n",bytes_written);
  
	return retval;
}/*}}}*/

/**
 *	cpad_nlcd_write
 *
 *      write a command to the non-LCD controller part of the cPad.  
 *      the first byte is 0x01 to select, 2nd byte is command:
 *        01 01 write parameters to EEPROM -- SORRY NOT SUPPORTED HERE (multi byte)
 *        01 02 read parameters from EEPROM
 *        01 03 write backlight state
 *        01 04 read backlight state
 *        01 05 write LCD on/off state
 *        01 06 read LCD on/off state
 *
 * for the 'read from EPROM' I get 
 *  "1 2 e0 ff 14 a 20 23 2a 10 2a 5 0 0 0 0 0 0 ff 35 0 0 0 0 0 0 0 0 0 0 0 0" 
 * don't know if that has a reversed section like the 1335 command, haven't tried writing it back.
 * found the 'write EEPROM params' first, toshiba was kind enough to fix it - not much interest here since.
 *
 *      this routine not intended for call from user space (should be called by ioctl, object locked)
 *      returns 0 on error or number of bytes written
 */
static int cpad_nlcd_write (struct usb_cpad *dev, const char cmd, const char val)/*{{{*/
{
	ssize_t bytes_written = 3;
	int retval = 0;

	if ((cmd < (char) 2) || (cmd > (char) 6)) {
		dbg("1335 nlcd invalid cmd"); 
		return 0;
	}

	dbg("nlcd minor %d, cmd = %d val = %d", dev->minor, cmd, val);

	// only called within ioctl, thus object already locked

	/* verify that the device wasn't unplugged */
	if (dev->udev == NULL) {
		retval = -ENODEV;
		goto exit;
	}

	/* see if we are already in the middle of a write */
	if (dev->write_urb->status == -EINPROGRESS) {
		dbg ("already writing");
		printk(" usb cpad -- already writing, not waiting -- fix me\n");
		//retval = -EBUSY;  // 0 means error
		goto exit;
	}

	((char *) dev->write_urb->transfer_buffer)[0] = SEL_CPAD;
	((char *) dev->write_urb->transfer_buffer)[1] = cmd;
	((char *) dev->write_urb->transfer_buffer)[2] = val;

	usb_cpad_debug_data (__FUNCTION__, bytes_written, dev->write_urb->transfer_buffer);

	//printk("cpad nlcd fbu\n");
	/* set up our urb */
	usb_fill_bulk_urb(dev->write_urb,
			  dev->udev, 
			  usb_sndbulkpipe(dev->udev, dev->bulk_out_endpointAddr),
			  dev->write_urb->transfer_buffer,
			  bytes_written,
			  cpad_write_bulk_callback,
			  dev);

	/* send the data out the bulk port */
	retval = usb_submit_urb(dev->write_urb);

	if (retval) {
		err("%s - failed submitting write urb, error %d", __FUNCTION__, retval);
	} else {
		retval = bytes_written;
	}
  
exit:
	//printk(KERN_DEBUG "cpad nlcd bulk write %d bytes\n",bytes_written);
	//printk("cpad nlcd bulk write %d bytes\n",bytes_written);

	return retval;
}
/*}}}*/

/*
 * cpad_eat_urb
 *  sem locked, just clear anything there
 */
static void cpad_eat_urb(struct usb_cpad *dev )/*{{{*/
{
	// do an immediate bulk read to get data from the device 
	usb_bulk_msg (dev->udev,
		      usb_rcvbulkpipe (dev->udev, dev->bulk_in_endpointAddr),
		      dev->bulk_in_buffer,
		      dev->bulk_in_size,
		      NULL,			// Don't care how much was really read.
		      HZ * 1);  		//was 10
}/*}}}*/

static void light_off(void *arg)/*{{{*/
{
	struct usb_cpad *dev = (struct usb_cpad *) arg;

	down (&dev->sem);

	//printk("loff 1\n"); 
	if (! cpad_nlcd_write(dev,CPAD_W_LIGHT, (char) 0))
		printk("error on timed_light_off\n");
	else
		cpad_eat_urb(dev);  // can't do as a timer fn -- fix with 2.5 re-write?

	up (&dev->sem);

	//printk("light off %u \n",jiffies); 
}/*}}}*/

/***
 * delayed light off for flash 
 **/
static void timed_light_off(unsigned long arg)/*{{{*/
{
	struct usb_cpad *dev = (struct usb_cpad *) arg;

	if (dev->tqueue.sync == 0) {
		dev->tqueue.routine = light_off;
		dev->tqueue.data = dev;
		schedule_task (&dev->tqueue);
	}
	else
	   	dbg ("sync error");
}/*}}}*/

/**
 *	cpad_write_bulk_callback
 */
static void cpad_write_bulk_callback (struct urb *urb)/*{{{*/
{
	struct usb_cpad *dev = (struct usb_cpad *)urb->context;

	dbg("minor %d", dev->minor);

	if ((urb->status != -ENOENT) && (urb->status != -ECONNRESET))
		dbg("nonzero write bulk status received: %d",urb->status);
}
/*}}}*/


/****
 * send cPad specific configuration urbs
 * rtm
 ***/
static void cpad_init(struct usb_cpad *dev, unsigned int ifnum)/*{{{*/
{
   //purb_t ctrl_urb;

   struct usb_device *udev=dev->udev;

   usb_set_interface(udev,ifnum,2);  /* cpad abs+lcd */
   //usb_set_interface(udev,ifnum,1);  /* cpad abs */


   /*  
   if (!(ctrl_urb = usb_alloc_urb(0))) {
      printk(KERN_DEBUG "cpad no mem for ctrl urb\n");
      return;
   }
   FILL_CONTROL_URB(ctrl_urb,dev,usb_sndctrlpipe(dev->udev),
   usb_control_msg(dev->udev, );
   */
}

/*}}}*/

/****
 * usbmouse suggests open()/close() needs to be controlled separately for input dev
 * rtm
 ***/
static int cpad_iopen(struct input_dev *dev)/*{{{*/
{
	struct usb_cpad *cpad = dev->private;

	if (cpad->mopen++)
		return 0;

	cpad->irq_urb->dev = cpad->udev;

	if (usb_submit_urb(cpad->irq_urb))
		return -EIO;

	return 0;
}/*}}}*/

static void cpad_iclose(struct input_dev *dev)/*{{{*/
{
	struct usb_cpad *cpad = dev->private;

	if (!--cpad->mopen)
		usb_unlink_urb(cpad->irq_urb);
}/*}}}*/

/****
 * Coordinate boundaries per synaptics suggestions, which seem close enough to
 * reality on my 5202.  This may vary between hardware, I don't know if it will
 * be by enough to warrant doing something adaptive..  we will see.
 ***/

#define ABSMIN_X	1472
#define ABSMIN_Y	1408
#define ABSMAX_X 	5472
#define ABSMAX_Y 	4448

/****
 * set up the features relevent to the selected operating mode
 * rtm
 ***/
static void announce_features(struct input_dev *idev)/*{{{*/
{
	idev->evbit[0] = BIT(EV_KEY);
	idev->keybit[LONG(BTN_MOUSE)] = BIT(BTN_LEFT) | BIT(BTN_RIGHT) | BIT(BTN_MIDDLE);
	idev->keybit[LONG(BTN_MOUSE)] |= BIT(BTN_SIDE) | BIT(BTN_EXTRA);

	if (absmouse != 0) {
		idev->evbit[0] |= BIT(EV_ABS);
		idev->absbit[0] = BIT(ABS_X) | BIT(ABS_Y) | BIT(ABS_PRESSURE);

		// We're expected to generate events in at least this range
		// according to input core lore.  Since the precise actual limits
		// may vary from device to device, let's set a reasonable value
		// here and presume anyone who cares will adapt to real limits
		// as they are seen.

		idev->absmin[ABS_X] = ABSMIN_X;
		idev->absmin[ABS_Y] = ABSMIN_Y;
		idev->absmax[ABS_X] = ABSMAX_X;
		idev->absmax[ABS_Y] = ABSMAX_Y;
		idev->absmax[ABS_PRESSURE] = 0xff;

		// This provides nice noise reduction and
		// an economy of more significant events.

		idev->absfuzz[ABS_X] = 8;
		idev->absfuzz[ABS_Y] = 8;
		idev->absfuzz[ABS_PRESSURE] = 4;
	}
	else {
		idev->evbit[0] |= BIT(EV_REL);
		idev->relbit[0] = BIT(REL_X) | BIT(REL_Y) |
				  BIT(REL_HWHEEL) | BIT(REL_WHEEL) | BIT(REL_MISC);
	}
}
/*}}}*/

/****
 * set up the input device system, tell it what we have 
 * rtm
 ***/
static int mouse_input(struct usb_cpad *dev)/*{{{*/
{
	char *buf;
	struct input_dev *idev  = &dev->idev;
	struct usb_device *udev = dev->udev;

	//printk(KERN_DEBUG "input device set up\n");
    
	announce_features( idev );

	idev->private = dev;
	idev->open = cpad_iopen;
	idev->close = cpad_iclose;

	idev->name = dev->mname;
	idev->idbus = BUS_USB;
	idev->idvendor = udev->descriptor.idVendor;
	idev->idproduct = udev->descriptor.idProduct;
	idev->idversion = udev->descriptor.bcdDevice;

	if (!(buf = kmalloc(63, GFP_KERNEL))) {
		err("input dev set up: out of memory!");
		return 0;
	}

	if (udev->descriptor.iManufacturer &&
	    usb_string(udev, udev->descriptor.iManufacturer, buf, 63) > 0) {
		strcat(dev->mname, buf);
	}

	if (udev->descriptor.iProduct && usb_string(udev, udev->descriptor.iProduct, buf, 63) > 0)
		sprintf(dev->mname, "%s %s", dev->mname, buf);

	if (!strlen(dev->mname))
		sprintf(dev->mname, "USB Mouse %04x:%04x", idev->idvendor, idev->idproduct);

	kfree(buf);

	return 1;
}
/*}}}*/

/***
 *   cpad_abs_irq
 *   handler for mouse data sent from cPad -- passed to input dev in the 
 *   int URB (absolute mouse coords version)
 *   
 */

static void cpad_abs_irq(struct urb *urb)/*{{{*/
{
	struct usb_cpad *dev = urb->context;
	unsigned char *data = dev->mdata;
	struct input_dev *idev = &dev->idev;

	if (urb->status != USB_ST_NOERROR) return;

	input_report_key(idev, BTN_LEFT,   data[1] & 0x04);
	input_report_key(idev, BTN_RIGHT,  data[1] & 0x01);
	input_report_key(idev, BTN_MIDDLE, data[1] & 0x08);
	//input_report_key(idev, BTN_SIDE,   data[1] & 0x02);
	//input_report_key(idev, BTN_EXTRA,  data[1] & 0x10);

	if (data[6] < min_press) {
		input_report_abs(idev, ABS_PRESSURE, 0);
	}
	else if ((data[0] & 0x0f) > 3) {
		input_report_abs(idev, ABS_X, (data[2] << 8) | data[3]);
		input_report_abs(idev, ABS_Y, ABSMIN_Y + ABSMAX_Y - ((data[4] << 8) | data[5]));
		input_report_abs(idev, ABS_PRESSURE, data[6]);
	}

#ifdef CPAD_DEBUG
   	if (debug)
		printk(KERN_DEBUG "cpad abs irq %5x %5x %5x %5x  %5x %5x %5x %5x \n",
			data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
#endif
}
/*}}}*/

/*
 * timed_event
 *
 * Execute actions in the event queue
 *
 */

static void timed_event(unsigned long arg)/*{{{*/
{
	struct input_dev *idev;
	int action,btn,rpoint = (int) arg;

	if (queue.action[rpoint]) {				// Something to do?
		idev   = &queue.device[rpoint]->idev;
		btn    = queue.action[rpoint]>>8;
		action = queue.action[rpoint] & 0xFF;
		input_report_key(idev, btn, action);
		if ((btn == BTN_LEFT) && (action == UP))	// Done waiting for a drag to happen
			queue.device[rpoint]->tap = 0;
	}
}
/*}}}*/

/*
 * schedule_event
 *
 * Put events into the queue for later action
 *
 */

static unsigned int schedule_event (struct usb_cpad *dev, int btn, int action, unsigned long delay)/*{{{*/
{
	unsigned int wpoint;

	wpoint = queue.wpoint;
	queue.device[wpoint] = dev;
	queue.action[wpoint] = (btn<<8) + action;
	queue.delay[wpoint].expires  = jiffies+delay;
	queue.delay[wpoint].function = timed_event;
	queue.delay[wpoint].data = (unsigned long) wpoint;
        add_timer(&queue.delay[wpoint]);
	queue.wpoint = (++queue.wpoint) & (NEVENT-1);

	return wpoint;
}
/*}}}*/

/*
 * unschedule_event
 *
 * Remove events in the queue if we've changed our mind
 * Lasciate ogni evento, voi ch'entrate...
 *
 */

static void unschedule_event(unsigned int event)/*{{{*/
{
	queue.action[event] = 0;
}
/*}}}*/

/***
 *   cpad_rel_irq
 *   handler for mouse data sent from cPad -- passed to input dev in the 
 *   int URB (relative mouse coords version)
 *   
 */

static void cpad_rel_irq(struct urb *urb)/*{{{*/
{
	struct usb_cpad *dev = urb->context;
	unsigned char *data = dev->mdata;
	struct input_dev *idev = &dev->idev;
	struct timeval tv;
	int d_us;

	if (urb->status != USB_ST_NOERROR) return;

	// It's OK to do MIDDLE and RIGHT buttons any ol' time
	input_report_key(idev, BTN_MIDDLE, data[1] & 0x08);
	input_report_key(idev, BTN_RIGHT,  data[1] & 0x01);

	if ((data[0] & 0x0f) > 3){
		// single finger contact

		if (data[6] > min_press) {
			// pressed hard enough

			__u32 currx,curry;
			__s32 sdifx,sdify;

			// Since the finger's already down, and if we're not
			// tappin'-n-draggin' we can safely transmit whatever
			// the LEFT button's real state is.
			if (!dev->tap)
				input_report_key(idev, BTN_LEFT,  data[1] & 0x04);

			currx = ((__u32)data[2] << 8) | data[3];
			curry = ABSMIN_Y + ABSMAX_Y - (((__u32)data[4] << 8) | data[5]);

			if (dev->touch == 0) {
				// first finger contact - set start position and time

				dev->lastx = currx;
				dev->lasty = curry;
				dev->touch = 1;
				do_gettimeofday(&dev->tv);

				// If we're expecting tap-n-drag, abandon delayed
				// button release, since we just got another
				// finger-down event before the last tap timed out
				if (dev->tap)
					unschedule_event(del_rel);
			}
			else {
				// finger motion - report relative change

				do_gettimeofday(&tv);
				d_us = ((tv.tv_sec - dev->tv.tv_sec)*1000000) +
				       (tv.tv_usec - dev->tv.tv_usec);

				// Don't start reporting motion right after a finger-down,
				// this helps avoid annoying pointer motion when you're
				// really trying to tap HERE, not THERE
				if (d_us > JITTER_TIME) {

					sdifx = abs(currx-dev->lastx) >> scale;
					sdifx += (sdifx>>scale) * addon;

					if (currx < dev->lastx) sdifx = -sdifx;

					sdify = abs(curry-dev->lasty) >> scale;
					sdify += (sdify>>scale) * addon;

					if (curry<dev->lasty) sdify = -sdify;

					if (sdifx)
					{
						input_report_rel(idev, REL_X, sdifx);
						dev->lastx = currx;

						// With a simpler acceleration model this
						// (instead of the above) should give nice
						// results. With the current code it gives
						// an interesting if not wholly unpleasant
						// effect depending on the current accel
						// value.  Probably too weird for most
						// people though.
						//dev->lastx += sdifx;
					}
					if (sdify)
					{
						input_report_rel(idev, REL_Y, sdify);
						dev->lasty = curry;
					}
				}
				else
				{
					dev->lastx = currx;
					dev->lasty = curry;
				}
			}

			// report pressure if user wants it
			input_report_rel(idev, REL_MISC, data[6]);
		}
	}
	else {
		// finger released (or button only)

		if (dev->touch == 0) {
			// Finger was up, this is a button only

			if (dev->tap) {
				// If we're waiting to see if there's a drag coming - it isn't

				if (data[1] & 0x04) {
					// If it's left button, press again slowly
					// to avoid X debounce

					// First make sure left button is released
					input_report_key(idev, BTN_LEFT, UP);
					// Abandon delayed button release
					unschedule_event(del_rel);
					schedule_event(dev, BTN_LEFT, DOWN, 1);
					dev->tap = 0;
				}
			}
			else {
				// Just do it
				input_report_key(idev, BTN_LEFT,  data[1] & 0x04);
			}
		}
		else {
			// Finger was down

			dev->touch = 0;

			do_gettimeofday(&tv);
			d_us = ((tv.tv_sec - dev->tv.tv_sec)*1000000) +
				(tv.tv_usec - dev->tv.tv_usec);

			if (d_us < TAP_TIME) {
				// finger press time was short => tap

				if (!dev->tap){
					// We're not waiting for a drag

					input_report_key(idev, BTN_LEFT, DOWN);

					// We don't know if this tap is going
					// to be followed by a drag so we can't
					// allow release until TND times out
					del_rel = schedule_event(dev, BTN_LEFT, UP, TND_JIFFIES);
				}
				else {
					// We were waiting for a drag but we got
					// a tap, the second of two, so just do it
					// and we'll start all over again

					input_report_key(idev, BTN_LEFT, UP);
					schedule_event(dev, BTN_LEFT, DOWN, 1);
					schedule_event(dev, BTN_LEFT, UP, 2);
				}
				dev->tap = 1-dev->tap;
			}
			else if (dev->tap) {
				// We're done dragging

				input_report_key(idev, BTN_LEFT, UP);
				dev->tap = 0;
			}
		}
	}

#ifdef CPAD_DEBUG
	if (debug)
		printk(KERN_DEBUG "cpad rel irq %5x %5x %5x %5x  %5x %5x %5x %5x \n",
			data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
#endif
}
/*}}}*/

/**
 *	cpad_probe
 *
 *	Called by the usb core when a new device is connected that it thinks
 *	this driver might be interested in.
 */
// in abs mode the cPad returns 8 bytes of mouse movement info

#define MDATA_SIZE 8
static void * cpad_probe(struct usb_device *udev, unsigned int ifnum, const struct usb_device_id *id)/*{{{*/
{
	struct usb_cpad *dev = NULL;
	struct usb_interface *interface;
	struct usb_interface_descriptor *iface_desc;
	struct usb_endpoint_descriptor *endpoint;
	int minor;
	int buffer_size;
	int i;
	char name[10];

#ifdef CPAD_DEBUG  
	printk(KERN_DEBUG "cpad probe here\n");
#endif

	/* See if the device offered us matches what we can accept */
	if ((udev->descriptor.idVendor != USB_CPAD_VENDOR_ID) ||
	    (udev->descriptor.idProduct != USB_CPAD_PRODUCT_ID)) {
		return NULL;
	}

	//usb_reset_device(udev);

	/* select a "subminor" number (part of a minor number) */
	down (&minor_table_mutex);

	for (minor = 0; minor < MAX_DEVICES && minor_table[minor] != NULL; ++minor);

	if (minor >= MAX_DEVICES) {
		info ("Too many devices plugged in, can not handle this device.");
		goto exit;
	}

	/* allocate memory for our device state and intialize it */
	dev = kmalloc (sizeof(struct usb_cpad), GFP_KERNEL);
	if (dev == NULL) {
		err ("Out of memory");
		goto exit;
	}
	memset(dev, 0, sizeof(struct usb_cpad));

	/* allocate memory for mouse report data and intialize it */
	dev->mdata = (char *) kmalloc (MDATA_SIZE, GFP_KERNEL);
	if (dev->mdata == NULL) {
		err ("Out of memory");
		goto exit;
	}
	memset(dev->mdata, 0, MDATA_SIZE);


	minor_table[minor] = dev;
	dev->minor = minor;
	init_MUTEX (&dev->sem);

	dev->udev = udev;
	for(i = 0; i < NEVENT; ++i)
		init_timer(&queue.delay[i]);
	init_timer(&dev->flash_delay);

	//usb_set_interface(udev,ifnum,2);  /* cpad abs+lcd */

	cpad_init(dev,ifnum);

	interface = &udev->actconfig->interface[ifnum];
	//dev->udev = udev;
	dev->interface = interface;
	iface_desc = &interface->altsetting[interface->act_altsetting];  // 2   /* cPad ABS+LCD rtm */

#ifdef CPAD_DEBUG
	printk(KERN_DEBUG "start endpoints\n");
#endif

	/* check out the endpoints */
	/* set up the endpoint information */

	for (i = 0; i < iface_desc->bNumEndpoints; ++i) {
		endpoint = &iface_desc->endpoint[i];

		if ((endpoint->bEndpointAddress & 0x80) &&
		    ((endpoint->bmAttributes & 3) == 0x02)) {

			/* we found a bulk in endpoint */
#ifdef CPAD_DEBUG
			printk(KERN_DEBUG "in endpoint: bulk\n");
#endif

			buffer_size = endpoint->wMaxPacketSize;
			dev->bulk_in_size = buffer_size;
			dev->bulk_in_endpointAddr = endpoint->bEndpointAddress;
			dev->bulk_in_buffer = kmalloc (buffer_size, GFP_KERNEL);
			if (!dev->bulk_in_buffer) {
				err("Couldn't allocate bulk_in_buffer");
				goto error;
			}
		}

		if (((endpoint->bEndpointAddress & 0x80) == 0x00) &&
		    ((endpoint->bmAttributes & 3) == 0x02)) {

			/* we found a bulk out endpoint */
#ifdef CPAD_DEBUG
			printk(KERN_DEBUG "out endpoint: bulk\n");
#endif

			dev->write_urb = usb_alloc_urb(0);
			if (!dev->write_urb) {
				err("No free urbs available");
				goto error;
			}

			buffer_size = endpoint->wMaxPacketSize;
			dev->bulk_out_size = buffer_size;

#ifdef CPAD_DEBUG
			printk(KERN_DEBUG "bulk out MaxPacket= %d\n",buffer_size);
#endif

			dev->bulk_out_endpointAddr = endpoint->bEndpointAddress;
			dev->bulk_out_buffer = kmalloc (buffer_size, GFP_KERNEL);
			if (!dev->bulk_out_buffer) {
				err("Couldn't allocate bulk_out_buffer");
				goto error;
			}
			usb_fill_bulk_urb(dev->write_urb,
					  udev, 
					  usb_sndbulkpipe(udev, endpoint->bEndpointAddress),
					  dev->bulk_out_buffer,
					  buffer_size,
					  cpad_write_bulk_callback,
					  dev);
		}

		if ((endpoint->bEndpointAddress & 0x80) &&
		    ((endpoint->bmAttributes & 3) == 0x03)) {

			int pipe;
			int maxp;

			/* we found an interrupt endpoint */
#ifdef CPAD_DEBUG
			printk(KERN_DEBUG "in endpoint: interrupt\n");
#endif

			dev->irq_urb = usb_alloc_urb(0);
			if (!dev->irq_urb) {
				err("No free urbs available");
				goto error;
			}

			pipe = usb_rcvintpipe(udev, endpoint->bEndpointAddress);
			maxp = usb_maxpacket(udev, pipe, usb_pipeout(pipe));

			buffer_size = endpoint->wMaxPacketSize;

			//printk(KERN_DEBUG "maxp=%i buffer_size=%i\n",maxp,buffer_size);
			/*
			dev->irq_in_size = buffer_size;
			dev->irq_in_endpointAddr = endpoint->bEndpointAddress;
			dev->irq_in_buffer = kmalloc (buffer_size, GFP_KERNEL);
			if (!dev->irq_in_buffer) {
			err("Couldn't allocate irq_in_buffer");
			goto error;
			}
			*/  

			usb_set_idle(udev, iface_desc->bInterfaceNumber, 0, 0);

			if (!mouse_input(dev)) 
				goto error;

			//printk(KERN_DEBUG "start fill_int_urb\n");

			if (absmouse != 0) {
				usb_fill_int_urb(dev->irq_urb,
						 udev,
						 pipe,
						 dev->mdata,
						 maxp > 8 ? 8 : maxp,
						 cpad_abs_irq,
						 dev,
						 endpoint->bInterval);
			}
			else {
				usb_fill_int_urb(dev->irq_urb,
						 udev,
						 pipe,
						 dev->mdata,
						 maxp > 8 ? 8 : maxp,
						 cpad_rel_irq,
						 dev,
						 endpoint->bInterval);
			}

			//printk(KERN_DEBUG "finished fiu\n");

			input_register_device(&dev->idev);

			printk( KERN_INFO "input%d: %s on usb%d:%d.%d\n",
				dev->idev.number,
				dev->mname,
				udev->bus->busnum,
				udev->devnum,
				ifnum);

			/*
			if (usb_submit_urb(dev->irq_urb)) {
				err("Couldn't submit irq_urb");
				goto error;
			}
			*/
		}
	}
  
#ifdef CPAD_DEBUG
	printk(KERN_DEBUG "endpoints done\n");
	printk(KERN_DEBUG "devfs...\n");
#endif
  
	/* initialize the devfs node for this device and register it */
	sprintf(name, "cpad%d", dev->minor);

	dev->devfs = devfs_register (usb_devfs_handle,
				     name,
				     DEVFS_FL_DEFAULT,
				     USB_MAJOR,
				     USB_CPAD_MINOR_BASE + dev->minor,
				     S_IFCHR | S_IRUSR | S_IWUSR | 
				     S_IRGRP | S_IWGRP | S_IROTH, 
				     &cpad_fops,
				     NULL);

	/* let the user know what node this device is now attached to */
	info ("USB Syn_cPad device now attached to USB cPad%d", dev->minor);
	goto exit;

error:
	cpad_delete (dev);
	dev = NULL;

exit:
	up (&minor_table_mutex);
	return dev;
}
/*}}}*/


/**
 *	cpad_ioctl
 */
static int cpad_ioctl (struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)/*{{{*/
{
	struct usb_cpad *dev;
	int retval = 0;
	unsigned char val = 0;
	int ival = 0;

	dev = (struct usb_cpad *)file->private_data;

	/* lock this object */
	down (&dev->sem); 

	/* verify that the device wasn't unplugged */
	if (dev->udev == NULL) {
		up (&dev->sem); 
		return -ENODEV;
	}

	dbg("minor %d, cmd 0x%.4x, arg %ld", dev->minor, cmd, arg);

	switch (cmd) {

	case CPAD_VERSION:
		if ((retval = put_user(DRIVER_NUM, (int *) arg))) 
			return retval;
		up (&dev->sem);
		return 0;
			
	case CPAD_CGID:
		if ((retval = put_user(dev->idev.idbus,     ((short *) arg) + 0))) return retval;
		if ((retval = put_user(dev->idev.idvendor,  ((short *) arg) + 1))) return retval;
		if ((retval = put_user(dev->idev.idproduct, ((short *) arg) + 2))) return retval;
		if ((retval = put_user(dev->idev.idversion, ((short *) arg) + 3))) return retval;
		up (&dev->sem);
		return 0;

	case CPAD_WLIGHT:
		if (get_user(val,(char *) arg)) { 
			up (&dev->sem);
			return -EFAULT;
		}
		else {
			//printk("wlight %d\n",(int) val);
			cpad_nlcd_write(dev,CPAD_W_LIGHT, val);
			cpad_eat_urb(dev);
			up (&dev->sem);
			return 0;
		}

	case CPAD_FLASH:
		if (get_user(ival,(int *) arg)) { 
			up (&dev->sem);
			return -EFAULT;
		}
		else {
			if (! cpad_nlcd_write(dev,CPAD_W_LIGHT, (char) 1) ) {
				printk("cpad flash failed to turn on\n");
				up (&dev->sem);   
				return 0; 
			}
			cpad_eat_urb(dev);  // won't work in timer'd call -- fix in 2.5 port?
			dev->flash_delay.function = timed_light_off;
			dev->flash_delay.data = (unsigned long) dev;
			mod_timer( &dev->flash_delay, jiffies + (unsigned long) ival );

			up (&dev->sem);

			return 0;
		}
	  
	case CPAD_WIMAGEL:
		up (&dev->sem);
		return 0;  // not yet supported -- use file i/o until I figure out bigger writes

	case CPAD_WLCD:
		if (get_user(val,(char *) arg)) {
			up (&dev->sem);
			return -EFAULT;
		}
		else {
			cpad_nlcd_write(dev,CPAD_W_LCD,val);
			cpad_eat_urb(dev);
			up (&dev->sem);
			return 0;
		}

	case CPAD_RLIGHT:
		cpad_nlcd_write(dev,CPAD_R_LIGHT,val);
		up (&dev->sem);
		return 0;

	case CPAD_RLCD:
		cpad_nlcd_write(dev,CPAD_R_LCD,val);
		up (&dev->sem);
		return 0;

	case CPAD_RESET:
		usb_reset_device(dev->udev);
		up (&dev->sem);
		return 0;
	  
	case CPAD_SET_SENS:
		if (get_user(ival,(int *) arg)) { 
			up (&dev->sem);
			return -EFAULT;
		}
		else {
			min_press = ival;
			up (&dev->sem);
			return 0;
		}

	case CPAD_SET_STROKE:
		if (get_user(ival,(int *) arg)) { 
			up (&dev->sem);
			return -EFAULT;
		}
		else {
			#define SCLIM	10
			int cnt;

			ival = cnt = (ival > 255) ? 255 : ((ival < 0) ? 0 : ival);
			scale = SCLIM;
			while (cnt > 1) scale--, cnt = cnt>>1;
			addon = ival&&((1<<(SCLIM-scale))-1);
			addon = (scale<=SCLIM/2) ? addon >> (SCLIM-2*scale) : addon << (2*scale-SCLIM);
			up (&dev->sem);
			return 0;
		}

	case CPAD_SET_ABS:
		if (get_user(val,(char *) arg)) { 
			up (&dev->sem);
			return -EFAULT;
		}
		else if (val == 1 && absmouse == 0) {
			unsigned long flags;

			spin_lock_irqsave(&dev->irq_urb->lock, flags);

			absmouse = 1; 
			announce_features(&dev->idev);
			dev->irq_urb->complete = cpad_abs_irq;

			spin_unlock_irqrestore(&dev->irq_urb->lock, flags);
		}
		else if (val != 1 && absmouse == 1) {
			unsigned long flags;

			spin_lock_irqsave(&dev->irq_urb->lock, flags);

			absmouse = 0; 
			announce_features(&dev->idev);
			dev->irq_urb->complete = cpad_rel_irq;

			spin_unlock_irqrestore(&dev->irq_urb->lock, flags);
		}
		up (&dev->sem);
		return 0;

	default:
		up (&dev->sem);
		/* return that we did not understand this ioctl call */
		return -ENOTTY;
	}
}
/*}}}*/

/**
 *	cpad_disconnect
 *
 *	Called by the usb core when the device is removed from the system.
 */
static void cpad_disconnect(struct usb_device *udev, void *ptr)/*{{{*/
{
	struct usb_cpad *dev;
	int minor;
	int i;

	dev = (struct usb_cpad *)ptr;
	
	for(i = 0; i < NEVENT; ++i)
		del_timer(&queue.delay[i]);
	del_timer(&dev->flash_delay);

	down (&minor_table_mutex);
	down (&dev->sem);
		
	minor = dev->minor;

	/* remove our devfs node */
	devfs_unregister(dev->devfs);

	/* if the device is not opened, then we clean up right now */
	if (!dev->open_count) {
		up (&dev->sem);
		cpad_delete (dev);
	}
	else {
		dev->udev = NULL;
		up (&dev->sem);
	}

	info("USB Syn_cPad #%d now disconnected", minor);
	up (&minor_table_mutex);
}

/*}}}*/

/**
 *	usb_cpad_init
 */
static int __init usb_cpad_init(void)/*{{{*/
{
	int result;

	/* register this driver with the USB subsystem */
	result = usb_register(&cpad_driver);
	if (result < 0) {
		err("usb_register failed for the "__FILE__" driver. Error number %d", result);
		return -1;
	}

	info(DRIVER_DESC " " DRIVER_VERSION);
	return 0;
}
/*}}}*/

/**
 *	usb_cpad_exit
 */
static void __exit usb_cpad_exit(void)/*{{{*/
{
	/* deregister this driver with the USB subsystem */
	usb_deregister(&cpad_driver);
}
/*}}}*/

module_init (usb_cpad_init);
module_exit (usb_cpad_exit);

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");


#if 0
/**
 *	cpad_write  -- not used anymore (writes anything to bulk endpoint)
 */
static ssize_t cpad_write (struct file *file, const char *buffer, size_t count, loff_t *ppos)/*{{{*/
{
	struct usb_cpad *dev;
  ssize_t bytes_written = 0;
  int retval = 0;

	dev = (struct usb_cpad *)file->private_data;


  dbg(__FUNCTION__ " - minor %d, count = %d", dev->minor, count);

// lock this object 
  down (&dev->sem);

// verify that the device wasn't unplugged 
  if (dev->udev == NULL) {
    retval = -ENODEV;
		goto exit;
  }

// verify that we actually have some data to write 
  if (count == 0) {
		dbg(__FUNCTION__ " - write request of 0 bytes");
		goto exit;
	}
  
// see if we are already in the middle of a write 
  if (dev->write_urb->status == -EINPROGRESS) {
    dbg (__FUNCTION__ " - already writing");
    if (file->f_flags & O_NONBLOCK) 
      retval = -EAGAIN;
    else 
      retval = -EBUSY;  // fixme: should wait for completion here
    goto exit;
	}
  
// we can only write as much as 1 urb will hold 
  bytes_written = (count > (size_t) dev->bulk_out_size) ? 
    dev->bulk_out_size : count;
  
// copy the data from userspace into our urb 
  if (copy_from_user(dev->write_urb->transfer_buffer, buffer, 
		     bytes_written)) {
    retval = -EFAULT;
    goto exit;
  }
  
  usb_cpad_debug_data (__FUNCTION__, bytes_written, 
		       dev->write_urb->transfer_buffer);

// set up our urb 
  FILL_BULK_URB(dev->write_urb, dev->udev, 
		      usb_sndbulkpipe(dev->udev, dev->bulk_out_endpointAddr),
		dev->write_urb->transfer_buffer, bytes_written,
		      cpad_write_bulk_callback, dev);
  
// send the data out the bulk port 
	retval = usb_submit_urb(dev->write_urb);
	if (retval) {
		err(__FUNCTION__ " - failed submitting write urb, error %d",
		    retval);
	} else {
		retval = bytes_written;
	}

exit:
	// unlock the device 
	up (&dev->sem);

	//printk(KERN_DEBUG "cpad bulk write %d bytes\n",bytes_written);

	return retval;

}/*}}}*/
#endif


/* vi:set sts=0 sw=8 noet foldmethod=marker: */
