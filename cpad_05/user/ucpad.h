/* 
 * Synaptics cPad(tm) definitions
 *
 * copyright (c) Rob Miller 2002
 */

#include "/usr/src/modules/cpad/cpad.h"

#define uchar unsigned char

/*
 * writing image data, 1 row of pixels at a time (pedantic for clarity):
 * 
 * USB URBs are 32 bytes (apparently)
 * each sequence must start with <select> and the 1335 command <MWRITE>
 * i.e. 0x02 0x42
 * followed by up to 30 bytes of bitmap pixels 
 *
 */

#define USB_BUF_LEN 32

/* 2 bytes for 1335 select and command */
#define SEL_MWRITE_LEN 2

/* 32 - 2 = 30 bytes = 240 pixels across */
#define IMGLLEN (USB_BUF_LEN-SEL_MWRITE_LEN)
#define IMGLINES 160
#define IMG_SIZE (IMGLINES * IMGLLEN)
#define IMGBITWID (IMGLLEN*8)

